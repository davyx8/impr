function blurImage = blurInFourierSpace(inImage,kernelSize)
n=kernelSize;
m1 = floor(n/2);
m2 = ceil(n/2);
k = 1:m2;
cf = [1 cumprod((n-k+1)./k)];
cf(n:-1:m1+2) = cf(1:m2-1);
kernel= conv2(cf,cf');
kernel=kernel./sum(sum(kernel)); 
[x,y]=size(inImage);
if (mod(x,2) == 0)
    padvec=zeros(y,1);
    inImage=vertcat(inImage,padvec');
    [x,y]=size(inImage);
end
if (mod(y,2) == 0)
    padvec=zeros(1,x);
    inImage=horzcat(inImage,padvec');
    [x,y]=size(inImage);
end
[kx,ky]=size(kernel);
paddedKer=padarray(kernel,[floor((x-kx)/2),floor((y-ky)/2) ]) ;
ft=DFT2(inImage);
kerFt=( DFT2(paddedKer));
blurImage = ft.*kerFt;
imshow(abs(ifftshift(IDFT2(blurImage))));
end

