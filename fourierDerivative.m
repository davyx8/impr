function magnitude = fourierDerivative( inImage)
ft=DFT2(inImage);
[w,h] = size(inImage);
u = [ 1:ceil(w/2)-1, -floor(w/2):0];
v= [ 0:ceil(h/2)-1,-floor(h/2):-1];
mat=repmat(u,h,1)';
mat2=repmat(v',1,w)';
yd=IDFT2((1/w)*2*pi* 1i*ft.*mat);
xd = IDFT2((1/h) * 2*pi * 1i*ft.*mat2);
res =sqrt(xd.^2 +yd.^2);
magnitude= (res);
figure;
imshow(magnitude);
end
