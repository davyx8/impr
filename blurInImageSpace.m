function blurImage = blurInImageSpace(inImage,kernelSize)
n=kernelSize;
m1 = floor(n/2);
m2 = ceil(n/2);
k = 1:m2;
cf = [1 cumprod((n-k+1)./k)];
cf(n+1:-1:m1+2) = cf(1:m2);
kernel= conv2(cf,cf');
kernel=kernel./sum(sum(kernel));

blurImage=conv2(inImage,kernel,'same');
imshow(blurImage);
end

