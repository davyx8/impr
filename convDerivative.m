function magnitude = convDerivative(inImage)
    xdev=[1 0 -1];
    ydev=xdev'
    x=conv2(inImage,xdev,'same');
    y=conv2(inImage, ydev,'same');
    size(x)
    size(y)
    magnitude=sqrt(x.^2 + y.^2 );
    figure;
    imshow(magnitude);
end