function fourierImage  =   DFT2(image)
fourierImage= (DFT(DFT(image).')).';
end