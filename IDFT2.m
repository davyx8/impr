function fourierImage  =   IDFT2(image)   
    n=size(image);
    fourierImage=(IDFT(IDFT(image).')).';
end