function fourierSignal  = DFT(signal)
    N= size (signal,2);
    u=0:N-1;
    x=u;
%     ee=size(conv2(x,u'))
%     ex=size(signal)
    
    fourierSignal=  (exp((-2 * 1i * pi * conv2(x,u'))/N)*signal.').';
end