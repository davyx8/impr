function signal  =   IDFT(fourierSignal)
    N = size(fourierSignal,2);
    n = N;
    u=0:N-1;
    x=-u;   
    signal= (fourierSignal*exp((2 * 1i * pi * conv2(x,u'))'/N))*(1/n);
end